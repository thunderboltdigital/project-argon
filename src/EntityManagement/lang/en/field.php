<?php

return [
    'saved' => 'Field saved!',
    'created' => 'Field created!',
    'deleted' => 'Field deleted!',
    'updated' => 'Field updated!',
];
