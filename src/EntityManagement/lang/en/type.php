<?php

return [
    'saved' => 'Content type saved!',
    'created' => 'Content type created!',
    'deleted' => 'Content type deleted!',
    'updated' => 'Content type updated!',
];
