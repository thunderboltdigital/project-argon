<?php

return [
    'saved' => 'Group saved!',
    'created' => 'Group created!',
    'deleted' => 'Group deleted!',
    'updated' => 'Group updated!',
];
