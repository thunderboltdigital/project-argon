<div id="medialib" class="modal fade" role="dialog" aria-labelledby="medialibraryLabel" aria-hidden="true">
    <input type="hidden" id="selectedMediaItem" value="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="medialibraryLabel">Media Library</h4>
            </div>
            <div class="modal-body">

                <div class="media-library" style="position: relative;">

                    <iframe id="medialib-iframe" frameborder="0"></iframe>

                </div>
            </div>
        </div>
    </div>
</div>