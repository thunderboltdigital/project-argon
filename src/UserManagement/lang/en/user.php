<?php

return [
    'saved' => 'Saved!',
    'created' => 'User created!',
    'deleted' => 'User deleted!',
];
