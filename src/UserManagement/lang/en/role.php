<?php

return [
    'saved' => 'Saved!',
    'created' => 'Role created!',
    'deleted' => 'Role deleted!',
];
