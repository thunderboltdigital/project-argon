<?php

namespace Escape\Argon\Authentication\Exceptions;

use Exception;

class AuthenticationException extends Exception
{
}
