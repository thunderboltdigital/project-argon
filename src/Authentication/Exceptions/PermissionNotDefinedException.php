<?php

namespace Escape\Argon\Authentication\Exceptions;

class PermissionNotDefinedException extends AuthenticationException
{
}
