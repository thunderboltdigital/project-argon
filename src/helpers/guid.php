<?php

function guid()
{
    return sprintf(
        '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,
        // 48 bits for "node"
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff)
    );
}

function toArray($var)
{
    if (is_array($var))
    {
        return $var;
    }

    $array = [];

    if (is_object($var))
    {
        foreach ($var as $key => $value)
        {
            $array[$key] = $value;
        }

        return $array;
    }

    if (is_null($var))
    {
        return $array;
    }

    if (is_scalar($var))
    {
        return [$var];
    }

    if (is_resource($var))
    {
        return $array;
    }

    return $array;
}

function spam_check($input, $min_time_to_fill=2)
{
    // If the bot catcher field is populated or the form was loaded and submitted in under $min_time_to_fill seconds
    // then we assume it has been submitted by a spam bot
    if (($input['catcher']!='') || ( (time()-$min_time_to_fill) < $input['timestamp'])) {
        // Add the users user agent to the input data and log the data
        $input['user_agent'] = @$_SERVER['HTTP_USER_AGENT'];

        \Log::info("Spam prevented.", $input);

        // Abort the process because this is spam
        abort(200, "Spam prevented.");
    }

    return true;
}

/**
 * Validate provided email and submits to it provided values.
 * In case email fails, logs values and emails digital team to handle the issue.
 * @param $email
 * @param array $input
 * @param string $subject - optional
 * @return bool
 */
function email_submission($email, array $input, $subject='')
{
    // validate the email supplied to make sure we can send values without a fail
    $validator = \Validator::make(['email' => $email], ['email' => 'required|email']);

    $timestamp = date('Y-m-d H:i:s');

    if ($validator->fails())
    {
        $e = new Exception("Invalid email address \"{$email}\" supplied to ".__METHOD__." in ".__FILE__);
        alert_escape($e, $timestamp);
        return false;
    }

    try
    {
        if (!$subject)
        {
            $subject = "Form submission @ {$timestamp}";
        }

        \Mail::send('argon::emails.template', ['content'=>$input], function ($message) use ($email, $subject)
        {
            $message->to($email)->subject($subject);
        });
    }
    catch (Exception $e)
    {
        alert_escape($e, $timestamp);
        return false;
    }

    return true;
}

/**
 * Log error and submitted input, then email Escape
 * @param Exception $error
 * @param string $timestamp - optional
 */
function alert_escape(Exception $e, $timestamp=null)
{
    if (is_null($timestamp))
    {
        $timestamp = date('Y-m-d H:i:s');
    }

    $error = format_error($e);
    $error['timestamp'] = $timestamp;

    \Log::error($error);

    email_escape($error);
}

/**
 * Email Escape using separate escape email config.
 * This is useful and independent form clients mailjet.
 * @param $data
 * @param string $subject - optional
 * @param string $template - optional
 * @param string $fromAddress - optional
 * @param string $fromName - optional
 * @param array $recepients - optional
 * @return bool
 */
function email_escape($data, $subject=null, $template='argon::emails.error', $fromAddress="error@the-escape.co.uk", $fromName="Error reporting", array $recepients=null)
{
    try
    {
        if (is_null($subject))
        {
            $subject = "Error @ ".url();
        }

        if (is_null($recepients))
        {
            $recepients = ['digital@the-escape.co.uk'];
        }

        // Backup your default mailer
        $backup = \Mail::getSwiftMailer();

        // Setup your mailer
        $transport = Swift_SmtpTransport::newInstance('in-v3.mailjet.com', 587, 'tls');
        $transport->setUsername('78de28444e70bc50ff74612ecc20caf5');
        $transport->setPassword('b22630a1b942e81558b3e40c1dd3fec3');
        // Any other mailer configuration stuff needed...

        $gmail = new Swift_Mailer($transport);

        // Set the mailer as gmail
        \Mail::setSwiftMailer($gmail);

        // Send your message
        \Mail::send($template, ['content'=>$data], function($message) use ($subject, $fromAddress, $fromName, $recepients)
        {
            $message
                ->from($fromAddress, $fromName)
                ->to($recepients)
                ->subject($subject);
        });

        // Restore your original mailer
        \Mail::setSwiftMailer($backup);

    }
    catch (Exception $e)
    {
        \Log::error(format_message($e->getMessage(), PHP_EOL));
        return false;
    }

    return true;
}


function format_message($message, $glue='<br>')
{
    if ($message)
    {
        if (is_array($message))
        {
            $message = array_filter($message);
            // compress array to string format
            $message = implode($glue, $message);
        }

        return $message;
    }

    return '';
}

/**
 * Prepare error data array
 * @param Exception $e
 * @return array $data
 */
function format_error(Exception $e)
{
    $data['msg'] 	    = $e->getMessage();
    $data['trace'] 		= $e->getTraceAsString();
    $data['line'] 		= $e->getLine();
    $data['file'] 		= $e->getFile();

    $data['post']       = empty($_POST) ? request()->all() : $_POST;
    $data['get']        = @$_GET;
    $data['files']      = @$_FILES;
    $data['session']    = @$_SESSION;
    $data['cookie']     = @$_COOKIE;

    $data['server']     = [];

    // filer server var as they will contain sensitive details from .env file
    $serverVariables = [
        'argv',
        'argc',
        'GATEWAY_INTERFACE',
        'SERVER_ADDR',
        'SERVER_NAME',
        'SERVER_SOFTWARE',
        'SERVER_PROTOCOL',
        'REQUEST_METHOD',
        'REQUEST_TIME',
        'REQUEST_TIME_FLOAT',
        'QUERY_STRING',
        'DOCUMENT_ROOT',
        'HTTP_ACCEPT',
        'HTTP_ACCEPT_CHARSET',
        'HTTP_ACCEPT_ENCODING',
        'HTTP_ACCEPT_LANGUAGE',
        'HTTP_CONNECTION',
        'HTTP_HOST',
        'HTTP_REFERER',
        'HTTP_USER_AGENT',
        'HTTPS',
        'REMOTE_ADDR',
        'REMOTE_HOST',
        'REMOTE_PORT',
        'REMOTE_USER',
        'REDIRECT_REMOTE_USER',
        'SCRIPT_FILENAME',
        'SERVER_ADMIN',
        'SERVER_PORT',
        'SERVER_SIGNATURE',
        'SCRIPT_NAME',
        'REQUEST_URI',
    ];

    foreach ($serverVariables as $serverVariable)
    {
        $data['server'][$serverVariable] = array_key_exists($serverVariable, $_SERVER) ? $_SERVER[$serverVariable] : '';
    }

    return $data;
}


/**
 * Easy pagination.
 * Returns array[
 *  'items_count' => total items count,
 *  'per_page' => items per page,
 *  'pages' => array of paginated items,
 *  'current_page_number' => current page number (offset),
 *  'page' => current page items,
 *  'pages_count' => total number of pages,
 *  'page_count' => total number of items on current page,
 *  'page_prev' => previous page number or null,
 *  'page_next' => next page number or null,
 *  'page_items_from' => number of item being returned from total items that is the first within current 'page' items,
 *  'page_items_to' => number of item being returned from total items that is the last within current 'page' items array,
 * ]
 * Return null if no items, or requested page number less than 1 to greater than 'pages_count'.
 *
 * @param array $items
 * @param int $per_page (-1 or any positive int, not 0)
 * @param null $current_page
 * @return null|pagination array
 */
function easyPagination(array $items, $per_page=10, $current_page_number=null)
{
    if ($items)
    {
        $pagination['items_count'] = count($items);
        $pagination['per_page'] = (preg_match('/^-1|[1-9][0-9]*$/', $per_page))
            ? (int) $per_page
            : trigger_error("Invalid 'per_page' argument supplied '{$per_page}'.", E_USER_ERROR);
        $pagination['pages'] = ($pagination['per_page']  > 0) ? array_chunk($items, $pagination['per_page']) : array_chunk($items, count($items));
        $pagination['pages_count'] = count($pagination['pages']);

        // get the integer value of a variable
        $current_page_number = ($current_page_number)
            ? $current_page_number
            : (isset($_GET['page']) ? $_GET['page'] : 1);

        // valid page can only be a non-negative integer
        $pagination['current_page_number'] = (preg_match('/^[1-9][0-9]*$/', $current_page_number)) ? (int) $current_page_number : null;

        if ($pagination['current_page_number'] > $pagination['pages_count'])
        {
            return null;
        }
        if ($pagination['current_page_number'] < 1)
        {
            return null;
        }

        // since arrays indexes are 0 based subscribe 1 from current page
        // and see if corresponding index exists in pages array
        // valid page can only be a non-negative integer
        $pagination['page'] = isset($pagination['pages'][$pagination['current_page_number']-1]) ? $pagination['pages'][$pagination['current_page_number']-1] : null;

        $pagination['page_count'] = count($pagination['page']);

        $pagination['page_prev'] = (($pagination_previous = $pagination['current_page_number'] - 1) < 1)
            ? null
            : $pagination_previous;

        $pagination['page_next'] = (($pagination_next = $pagination['current_page_number'] + 1) > $pagination['pages_count'])
            ? null
            : $pagination_next;

        $page_offset = ($pagination['current_page_number'] * $pagination['per_page']) - $pagination['per_page'];

        $pagination['page_items_from'] = $page_offset + 1;

        $pagination['page_items_to'] = $page_offset + $pagination['page_count'];

        return $pagination;
    }

    return null;
}


function paginationPresenter($pagination, $hellip='...', $minThreshold=1, $maxThreshold=2, callable $callback=null)
{
    $output = [];

    $current_page_number = $pagination['current_page_number'];

    $maxThreshold = $pagination['pages_count'] - $maxThreshold;

    $range = range(1, $pagination['pages_count']);


    foreach ($range as $num)
    {
        if ($current_page_number == $num)
        {
            $output[] = $num;
            continue;
        }

        if ($current_page_number == ($num-1))
        {
            $output[] = $num;
            continue;
        }

        if (($num+1) <= $pagination['pages_count'] && $current_page_number == ($num+1))
        {
            $output[] = $num;
            continue;
        }

        if ($num <= $minThreshold)
        {
            $output[] = $num;
            continue;
        }

        if ($num > $maxThreshold)
        {
            $output[] = $num;
            continue;
        }

        $output[] = $hellip;
    }

    $v = '';

    // collapse duplicate segments of  $hellip values into single instance
    foreach ($output as $key => $value)
    {
        if ($value != $v)
        {
            $v = $value;
        }
        else
        {
            unset($output[$key]);
        }
    }

    // if defined apply callback to each output item
    if ($callback)
    {
        foreach ($output as $key => &$value)
        {
            $value = call_user_func_array($callback, [$value, $hellip, $current_page_number]);
        }
    }

    return $output;
}


function getUrlWithQueryString(array $set=[], array $unset=[], $url=null, $encode=true)
{
    if ($url === null) {
        $url = $_SERVER['REQUEST_URI'];
    }

    $url = parse_url($url, PHP_URL_PATH);
    $url = rtrim($url, '?&');

    parse_str($_SERVER['QUERY_STRING'], $qs);

    $qs = array_merge($qs, $set);

    foreach ($unset as $key) {
        unset($qs[$key]);
    }

    if ($qs) {
        $url .= (strpos($url, '?')) ? '&' : '?';
        $url .= http_build_query($qs);
    }

    if (!$encode)
    {
        $url = urldecode($url);
        $url = preg_replace('/\s+/', '+', $url);
    }

    return $url;
}

function getUrlWithQueryStringNoEncoding(array $set=[], array $unset=[], $url=null)
{
    return getUrlWithQueryString($set, $unset, $url, false);
}

function getUrlNoQueryString($url=null)
{
    if (is_null($url))
    {
        $url = $_SERVER['REQUEST_URI'];
    }

    return parse_url($url, PHP_URL_PATH);
}

/**
 * Sorts collection looking at CMS field values
 * @param $collection
 * @param $field
 * @param string $direction asc|desc
 * @return mixed
 */
function sortByField($collection, $field, $direction='asc')
{
    $temp = $collection->splice(0, $collection->count());

    $directions = ['asc', 'desc'];
    if (!in_array($direction, $directions))
    {
        throw new \RuntimeException("Invalid sorting direction. Expected asc|desc, '$direction' given.");
    }

    $sorted = [];

    foreach($temp as $item)
    {
        $f = $item->field($field);

        if ($f instanceof DatetimeFieldValue)
        {
            $v = $f->timestamp;
        }
        else
        {
            $v = (string) $f;
        }

        $sorted[] = $v;
    }

    natcasesort($sorted);

    if ($direction == 'desc')
    {
        $sorted = array_reverse($sorted);
    }

    foreach ($sorted as $sortedValue)
    {
        foreach($temp as $i => $item)
        {
            $itemValue = (string) $item->field($field);

            if ($sortedValue == $itemValue)
            {
                $collection->push($item);
                $temp->forget($i);
                break;
            }
        }
    }

    return $collection;
}
